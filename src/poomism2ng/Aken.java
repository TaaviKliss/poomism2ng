package poomism2ng;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.MouseEvent; // sain infot Java foorumitest ning Youtube'ist, enne ei olnud kunagi kasutanud
import java.awt.event.MouseListener;


import javax.swing.ImageIcon;
import javax.swing.JPanel;


public class Aken extends JPanel implements MouseListener{
	
	
	private static final long serialVersionUID = 1L; // oli vaja MouseListener'i implementeerimiseks
	public Image v6taTaustapilt;
	public ImageIcon taustapilt; 
	
	public Image v6taT2hestik;
	public ImageIcon t2hestik; 
	public int valeVastus;

	S6naKontroll kontroll;
	
	public Aken() {

		kontroll = new S6naKontroll();
		kontroll.valiS6na();
		taustapilt = new ImageIcon("pildifailid/taust.png"); //et oleks ilus taust, t�mbasin Google'st �he .png formaadis pildi alla
		v6taTaustapilt = taustapilt.getImage();
		
		t2hestik = new ImageIcon("pildifailid/t2hestik.png"); //tundus lahedam, kui t�he peale klikkida, mitte seda tr�kkida
		v6taT2hestik = t2hestik.getImage();
		
		addMouseListener(this);	
		valeVastus = 0;
		}
	
	
	public void paint(Graphics g) { // k�ige aegan�udvam osa, joonistuspraktikumidest eeskuju v�etud

		g.drawImage(v6taTaustapilt,0,0,1000,700,null);
		g.setColor(Color.WHITE);
		g.drawImage(v6taT2hestik,660,10,300,300, null);
		g.setFont(new Font("Arial",Font.ITALIC,70));
		g.drawChars(kontroll.t2hed, 0, kontroll.t2htedeArv.length, 380, 360);
		g.setColor(Color.black);
		g.fillRect(450, 30, 120, 40);
		g.setColor(Color.white);
		g.setFont(new Font("Arial",Font.BOLD,18));
		g.drawString("UUS M�NG", 460, 55);

		if(kontroll.valePakkumine == 1){
			g.setColor(Color.black);
			g.fillOval(245, 190, 60, 60); //pea
			g.setColor(Color.black);
			g.fillRect(450, 30, 120, 40);
			g.setColor(Color.white);
			g.setFont(new Font("Arial",Font.BOLD,18));
			g.drawString("UUS M�NG", 460, 55);
		}
		if(kontroll.valePakkumine == 2){
			g.setColor(Color.black);
			g.fillOval(245, 190, 60, 60);	//pea
			g.fillRect(270, 210, 10, 120);	//kere
			g.setColor(Color.black);
			g.fillRect(450, 30, 120, 40);
			g.setColor(Color.white);
			g.setFont(new Font("Arial",Font.BOLD,18));
			g.drawString("UUS M�NG", 460, 55);
		}
		if(kontroll.valePakkumine == 3){
			g.setColor(Color.black);
			g.fillOval(245, 190, 60, 60);	//pea
			g.fillRect(270, 210, 10, 120);	//kere
			g.fillOval(265, 260, 100, 10);	//parem k�si
			g.setColor(Color.black);
			g.fillRect(450, 30, 120, 40);
			g.setColor(Color.white);
			g.setFont(new Font("Arial",Font.BOLD,18));
			g.drawString("UUS M�NG", 460, 55);
		}
		if(kontroll.valePakkumine == 4){		
			g.setColor(Color.black);
			g.fillOval(245, 190, 60, 60);	//pea
			g.fillRect(270, 210, 10, 120);	//kere
			g.fillOval(265, 260, 100, 10);	//parem k�si
			g.fillOval(170, 260, 100, 10);	//vasak k�si
			g.setColor(Color.black);
			g.fillRect(450, 30, 120, 40);
			g.setColor(Color.white);
			g.setFont(new Font("Arial",Font.BOLD,18));
			g.drawString("UUS M�NG", 460, 55);
		}
		if(kontroll.valePakkumine == 5){	
			g.setColor(Color.black);
			g.fillOval(245, 190, 60, 60);	//pea
			g.fillRect(270, 210, 10, 120);	//kere
			g.fillOval(265, 260, 100, 10);	//parem k�si
			g.fillOval(170, 260, 100, 10);	//vasak k�si
			g.fillOval(270, 320, 90, 20);	//parem jalg. Jalg on sellep�rast selline, et ei tahtnud peenikest joont
			g.setColor(Color.black);
			g.fillRect(450, 30, 120, 40);
			g.setColor(Color.white);
			g.setFont(new Font("Arial",Font.BOLD,18));
			g.drawString("UUS M�NG", 460, 55);
		}
		if(kontroll.valePakkumine == 6){	
			g.setColor(Color.white);
			g.fillOval(245, 190, 60, 60);	//pea
			g.fillRect(270, 210, 10, 120);	//kere
			g.fillOval(265, 260, 100, 10);	//parem k�si
			g.fillOval(170, 260, 100, 10);	//vasak k�si
			g.fillOval(270, 320, 90, 20);	//parem jalg
			g.fillOval(190, 320, 90, 20);	//vasak jalg
			g.setColor(Color.black);
			g.fillRect(450, 30, 120, 40);
			g.setColor(Color.white);
			g.setFont(new Font("Arial",Font.BOLD,18));
			g.drawString("UUS M�NG", 460, 55);
		}
		if(kontroll.tulemus == true){
			g.setColor(Color.red);
			g.drawString("V�ITSID", 500, 400);
			g.setColor(Color.black);
			g.fillRect(450, 30, 120, 40);
			g.setColor(Color.white);
			g.setFont(new Font("Arial",Font.BOLD,18));
			g.drawString("UUS M�NG", 460, 55);
		
		}
		if(kontroll.valePakkumine == 6){
			g.setColor(Color.red);
			g.drawString("POODUD", 500, 400);
			g.setColor(Color.black);
			g.fillRect(450, 30, 120, 40);
			g.setColor(Color.white);
			g.setFont(new Font("Arial",Font.BOLD,18));
			g.drawString("UUS M�NG", 460, 55);
		}
		
		repaint();
	}

	@Override
	public void mouseClicked(MouseEvent klikk) { //puhtalt ise juurde �pitud osa, samuti v�ga aegan�udev
	
		int mX = klikk.getX(); 
		int mY = klikk.getY();
	 ///the If statement below will activated when man is hanged or user wins 
		if(mX > 450 && mX < 570 && mY > 30 && mY < 70 && (kontroll.valePakkumine >= 0 || kontroll.tulemus == true)){
			kontroll.valePakkumine = 0;
			kontroll.valiS6na();
			kontroll.tulemus = false;
			for(int i = 0; i < 26; i++){ 
				kontroll.kasutatudT2hed[i] = '-';
			}
		}
		
		 // panen paika, kuhu klikkides missugune t�ht tuleb
		
		// Esimene rida
		if(mX > 659 && mX < 703 && mY > 9 && mY < 86){
			char ch = 'a';
			kontroll.kontrolli(ch);
			}
		if(mX > 702 && mX < 745 && mY > 9 && mY < 86){
			char ch = 'b';
			kontroll.kontrolli(ch);
			}
		if(mX > 744 && mX < 787 && mY > 9 && mY < 86){
			char ch = 'c';
			kontroll.kontrolli(ch);
			}
		if(mX > 786 && mX < 829 && mY > 9 && mY < 86){
			char ch = 'd';
			kontroll.kontrolli(ch);
			}
		if(mX > 828 && mX < 871 && mY > 9 && mY < 86){
			char ch = 'e';
			kontroll.kontrolli(ch);
			}
		if(mX > 870 && mX < 913 && mY > 9 && mY < 86){
			char ch = 'f';  // f being click
			kontroll.kontrolli(ch);
			}
		if(mX > 912 && mX < 955 && mY > 9 && mY < 86){
			char ch = 'g';
			kontroll.kontrolli(ch);
			}
		
		// Teine rida
		
		if(mX > 659 && mX < 703 && mY > 85 && mY < 161){
			char ch = 'h';
			kontroll.kontrolli(ch);
			}
		if(mX > 702 && mX < 745 && mY > 85 && mY < 161){
			char ch = 'i';
			kontroll.kontrolli(ch);
			}
		if(mX > 744 && mX < 787 && mY > 85 && mY < 161){
			char ch = 'j';
			kontroll.kontrolli(ch);
			}
		if(mX > 786 && mX < 829 && mY > 85 && mY < 161){
			char ch = 'k';
			kontroll.kontrolli(ch);
			}
		if(mX > 828 && mX < 871 && mY > 85 && mY < 161){
			char ch = 'l';
			kontroll.kontrolli(ch);
			}
		if(mX > 870 && mX < 913 && mY > 85 && mY < 161){
			char ch = 'm';
			kontroll.kontrolli(ch);
			}
		if(mX > 912 && mX < 955 && mY > 85 && mY < 161){
			char ch = 'n';
			kontroll.kontrolli(ch);
			}
		
		// Kolmas rida
		
		if(mX > 659 && mX < 703 && mY > 160 && mY < 236){
			char ch = 'o';
			kontroll.kontrolli(ch);
			}
		if(mX > 702 && mX < 745 && mY > 160 && mY < 236){
			char ch = 'p';
			kontroll.kontrolli(ch);
			}
		if(mX > 744 && mX < 787 && mY > 160 && mY < 236){
			char ch = 'q';
			kontroll.kontrolli(ch);
			}
		if(mX > 786 && mX < 829 && mY > 160 && mY < 236){
			char ch = 'r';
			kontroll.kontrolli(ch);
			}
		if(mX > 828 && mX < 871 && mY > 160 && mY < 236){
			char ch = 's';
			kontroll.kontrolli(ch);
			}
		if(mX > 870 && mX < 913 && mY > 160 && mY < 236){
			char ch = 't';
			kontroll.kontrolli(ch);
			}
		if(mX > 912 && mX < 955 && mY > 160 && mY < 236){
			char ch = 'u';
			kontroll.kontrolli(ch);
			}
		
		// Viimane rida
				
		if(mX > 702 && mX < 745 && mY > 235 && mY < 311){
			char ch = 'v';
			kontroll.kontrolli(ch);
			}
		if(mX > 744 && mX < 787 && mY > 235 && mY < 311){
			char ch = 'w';
			kontroll.kontrolli(ch);
			}
		if(mX > 786 && mX < 829 && mY > 235 && mY < 311){
			char ch = 'x';
			kontroll.kontrolli(ch);
			}
		if(mX > 828 && mX < 871 && mY > 235 && mY < 311){
			char ch = 'y';
			kontroll.kontrolli(ch);
			}
		if(mX > 870 && mX < 913 && mY > 235 && mY < 311){
			char ch = 'z';
			kontroll.kontrolli(ch);
			
			
		}
	}
	@Override
	public void mouseEntered(MouseEvent klikk) { //MouseEvent'i oluline osa, muidu ei toimiks
				
	}
	@Override
	public void mouseExited(MouseEvent klikk) {
			
	}
	@Override
	public void mousePressed(MouseEvent klikk) {
		
			
	}
	@Override
	public void mouseReleased(MouseEvent klikk) {
				
	}

}
